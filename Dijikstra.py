import sys, csv

# Node Class to hold the network's node and it's adjacent nodes (neighbors)
class Node:

    # Constructor to initialize node object with node name and create a dict to hold its neighbor and cost 
    def __init__(self,key):
        self.key = key
        self.neighbors = {}

    def addNeighbor(self, node, cost):
        self.neighbors[node] = cost

# Graph Class to hold the network nodes
class Graph:
    # Constructor with number of nodes and node Name to Node Mapping
    def __init__(self):
        self.nodeMap = {}
        self.nodes = 0

    # adds node to the graph
    def addNode(self, key):
        self.nodes += 1
        self.nodeMap[key] = Node(key)

    # Returns the node by name
    def getNode(self, key):
        if key in self.nodeMap:
            return self.nodeMap[key]
        else:
            return None

    # Adds edges to the graph
    def addEdge(self, source, destination, weight):
        if source not in self.nodeMap:
            self.addNode(source)
        if destination not in self.nodeMap:
            self.addNode(destination)
        self.nodeMap[source].addNeighbor(self.nodeMap[destination], weight)

    # returns the nodes key set
    def getNodes(self):
        return set(self.nodeMap.keys())

# main function: this is where the start of the program execution occurs
if __name__ == "__main__":
    
    # read the filename from the command line arguments
    filename = sys.argv[1]
    n = input("Please, provide the node's name: ")
   
    # temp dict to map the csv index to the node names
    nodeIndex = {}
    graph = Graph()

    # open csv file
    with open(filename) as csvFile:

        # read the whole csv file into data
        data = csv.reader(csvFile)

        # loop over each row data
        for ri, row in enumerate(data):

            #loop over each column
            for ci, column in enumerate(row):
                if ci > 0:
                    # initialize the graph node names from the heading and skip 
                    if ri == 0:
                        graph.addNode(column)
                        nodeIndex[ci] = column
                        continue
                    
                    # add edges to the graph
                    graph.addEdge(row[0], nodeIndex[ci], int(column))
    
    # Dijikstra Algorithm: The following code follows the algorithm from the class slides
   
    # 1. initialize 
    N = set(n) # N prime: set of nodes whose shortest-path are already known
    D = {}     # stores current cost of least-cost path for each node
    p = {}     # stores the shortest path for each node
    node = graph.getNode(n) # get Node from node name
    trace = n  # track the shortest path

    for name, cost in node.neighbors.items():
        D[name.key] = cost
        p[name.key] = n
    
    # 2. repeat
    while (len(N) < graph.nodes): # loop until all nodes are visited
        w = graph.getNodes().difference(N) # find w not in N
        miniumumD = min(D[i] for i in w) # find min(D(w))

        # find w not in N with D(w) as minimum
        for key, cost in D.items():
            if cost == miniumumD and key in w:
                w = key

        p[w] += w       # add to path
        trace = p[w]    # shortest path known
        N.add(w)        # add to visited nodes
        node = graph.getNode(w)

        for name, cost in node.neighbors.items():
            if name.key not in N:
                D[name.key] = min(D[name.key], D[w] + cost) # D(v) = min(D(v), D(w) + c(w,v))
                if D[name.key] == D[w] + cost:
                    p[name.key] = trace

    print(f'Shortest path tree node {n}: \n', p)
    print(f'Costs of least-cost paths for node {n}:\n', D)
